import glob
from shutil import copyfile

emotions = ["neutral", "anger", "contempt", "disgust", "fear", "happy", "sadness", "surprise"]  # Define emotion order
participants = glob.glob("./source_emotion/*")  # Returns a list of all folders with participant numbers

print len(participants)
#print participants

for x in participants:
    part = "%s" % x[-4:]  # store current participant number
    print part
    # print x
    print ("%s/*" % x)
    for sessions in glob.glob("%s/*" % x):  # Store list of sessions for current participant
        print "session"+sessions
        for files in glob.glob("%s/*" % sessions):
            #print "files"+files
            current_session = files[22:-30]
            #print "cs"+current_session
            file = open(files, 'r')

            emotion = int(float(file.readline()))  # emotions are encoded as a float, readline as float, then convert to integer.
            #print emotion
            #print "sfm"+("./source_images/%s/%s/*"  % (part, current_session))
            sourcefile_emotion = glob.glob("./source_images/%s/%s/*" % (part, current_session))[-1]  # get path for last image in sequence, which contains the emotion
            sourcefile_neutral = glob.glob("./source_images/%s/%s/*" % (part, current_session))[0]  # do same for neutral image

            dest_neut = "./sorted_set/neutral/%s" % sourcefile_neutral[25:]  # Generate path to put neutral image
            dest_emot = "./sorted_set/%s/%s" % (emotions[emotion], sourcefile_emotion[25:])  # Do same for emotion containing image

            print "dn" + dest_neut
            print "de"+dest_emot
            print "sn"+sourcefile_neutral
            copyfile(sourcefile_neutral, dest_neut)  # Copy file
            copyfile(sourcefile_emotion, dest_emot)  # Copy file


